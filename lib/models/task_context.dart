
import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:todoey_flutter/models/task.dart';

class TaskContext extends ChangeNotifier {
  List<Task> _tasks = [
    Task(name: 'Buy milk'),
    Task(name: 'Buy eggs'),
    Task(name: 'Buy bread'),
  ];

  int get taskCount {
    return _tasks.length;
  }

  void addTaskCallback(String newTaskTitle) {
    if(newTaskTitle.isNotEmpty) {
      _tasks.add(Task(name: newTaskTitle));
      notifyListeners();
    }
  }

  void toggleTaskCallback(int index) {
    _tasks[index].toggleDone();
    notifyListeners();
  }

  UnmodifiableListView<Task> get tasks {
    return UnmodifiableListView(_tasks);
  }

  Task removeTask(int index) {
    Task task = _tasks.removeAt(index);
    notifyListeners();
    return task;
  }
}