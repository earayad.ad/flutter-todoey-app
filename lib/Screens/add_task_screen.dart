import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../models/task.dart';

class AddTaskScreen extends StatelessWidget {
  var addTaskCallback;
  String newTaskTitle = '';

  AddTaskScreen({Key? key, required this.addTaskCallback}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xff757575),
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Container(
          padding: const EdgeInsets.fromLTRB(40, 40, 40, 40),
          child: Column(
            children: [
              const Text('AddTask',
                  style: TextStyle(
                    color: Color(0xff82d2fd),
                    fontSize: 30,
                    fontWeight: FontWeight.w500,
                  )),
              TextField(
                autofocus: true,
                textAlign: TextAlign.center,
                decoration: const InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xff82d2fd)),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xff82d2fd)),
                  ),
                ),
                onChanged: (newText) {
                  newTaskTitle = newText;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () => {
                    addTaskCallback(newTaskTitle),
                    Navigator.pop(context)
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xff82d2fd),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  child: const Text('Add',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}