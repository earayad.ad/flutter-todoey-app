import 'package:flutter/material.dart';

import '../models/task.dart';

class TaskTile extends StatelessWidget {
  const TaskTile({super.key, required this.taskName, required this.checkboxState, required this.checkboxCallback, required this.onLongPressCallback});

  final String taskName;
  final bool checkboxState;
  final Function checkboxCallback;
  final Function onLongPressCallback;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: () {
        onLongPressCallback();
        // Provider.of<TaskContext>(context, listen: false).deleteTaskCallback(taskName);
      },
      title: Text(
        taskName,
        style: TextStyle(
          color: checkboxState ? Colors.grey : Colors.black,
          decoration: checkboxState ? TextDecoration.lineThrough : null,
        ),
      ),
      trailing: Checkbox(
        value: checkboxState,
        onChanged: (value) {
          checkboxCallback();
        },
      ),
    );
  }
}
