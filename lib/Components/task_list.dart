import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:todoey_flutter/Components/task_tile.dart';
import 'package:provider/provider.dart';
import '../models/task.dart';
import '../models/task_context.dart';

class TaskList extends StatelessWidget {
  const TaskList({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskContext>(
      builder:(context, taskContext, child) {
        return ListView.builder(itemBuilder: (context, index) {
          return TaskTile(
            taskName: taskContext.tasks[index].name,
            checkboxState: taskContext.tasks[index].isDone,
            checkboxCallback: () {
              taskContext.toggleTaskCallback(index);
            },
            onLongPressCallback: () {
              taskContext.removeTask(index);
            },
          );
        },
          itemCount: taskContext.taskCount,
        );
      }
    );
  }
}