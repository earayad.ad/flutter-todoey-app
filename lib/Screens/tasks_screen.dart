
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todoey_flutter/Components/task_tile.dart';
import 'package:todoey_flutter/models/task_context.dart';

import '../Components/task_list.dart';
import '../models/task.dart';
import 'add_task_screen.dart';
import 'package:provider/provider.dart';

class TasksScreen extends StatelessWidget {
  const TasksScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskContext>(
      builder: (context, taskContext, child) {
        return Scaffold(
          backgroundColor: Colors.lightBlueAccent,
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              showModalBottomSheet(context: context, builder: (context) => AddTaskScreen(addTaskCallback: taskContext.addTaskCallback));
            },
            backgroundColor: Colors.lightBlueAccent,
            child: const Icon(Icons.add),
          ),
          body: Container(
            padding: const EdgeInsets.only(top: 60, left: 0, right: 0, bottom: 0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: const EdgeInsets.only(left: 20, top: 0, right: 0, bottom: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CircleAvatar(
                            radius: 60,
                            backgroundColor: Colors.white,
                            child: Icon(Icons.list,
                              size: 60,
                              color: Colors.lightBlueAccent,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Text('Todoey',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 50.0,
                                fontWeight: FontWeight.w700,
                              )
                          ),
                          Text('${taskContext.taskCount} task',
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              )
                          ),
                        ],
                      )
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 0),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                      ),
                      child: TaskList(),
                    ),
                  ),
                ]
            ),
          ),
        );
      }
    );
  }
}




